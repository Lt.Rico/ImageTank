﻿using System;
using Windows.Devices.Gpio;

namespace Imagetank
{
    class ButtonListener
    {

        private const int BUTTON_PIN1 = 26;
        private static GpioPin buttonPin1;
        private const int BUTTON_PIN2 = 20;
        private static GpioPin buttonPin2;

        private static bool error = false;
        private static bool lck = false;

        public static void init()
        {
            var gpio = GpioController.GetDefault();
            if (gpio == null)
                return;
            buttonPin1 = gpio.OpenPin(BUTTON_PIN1);
            if (buttonPin1.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                buttonPin1.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                buttonPin1.SetDriveMode(GpioPinDriveMode.Input);

            buttonPin1.DebounceTimeout = TimeSpan.FromMilliseconds(50);
            buttonPin1.ValueChanged += button1Pushed;

            buttonPin2 = gpio.OpenPin(BUTTON_PIN2);
            if (buttonPin2.IsDriveModeSupported(GpioPinDriveMode.InputPullUp))
                buttonPin2.SetDriveMode(GpioPinDriveMode.InputPullUp);
            else
                buttonPin2.SetDriveMode(GpioPinDriveMode.Input);

            buttonPin2.DebounceTimeout = TimeSpan.FromMilliseconds(50);
            buttonPin2.ValueChanged += button2Pushed;

        }

        private static void button1Pushed(GpioPin sender, GpioPinValueChangedEventArgs e)
        {
            if (e.Edge == GpioPinEdge.FallingEdge && !lck)
            {
                lck = true;
                Log.writeLine("Kopieren Taste gedrückt");
                if(error)
                {
                    Log.writeLine("Error zurückgesetzt");
                    LEDHandler.setBlink(0, 0);
                    LEDHandler.setBlink(1, 0);
                    error = false;
                }else
                {

                    var tank = DeviceHelper.getTank();
                    var devices = DeviceHelper.getDevices();
                    if (tank == null)
                    {
                        Log.writeLine("Keinen Tank-Speicher gefuden");
                        LEDHandler.setBlink(0, 0.15, 3);
                    }
                    else
                    {
                        if(devices.Count > 0)
                        {
                            LEDHandler.setBlink(1, 0.75);
                            foreach (var device in devices)
                            {
                                var name = DeviceHelper.getName(device);
                                var dest = tank.CreateFolderAsync(name, Windows.Storage.CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
                                Log.writeLine("Kopiere Daten von Gerät: " + name);
                                if (!FileOperations.copy(device, dest, Settings.getSettingBool("delorg")))
                                {
                                    Log.writeLine("Nicht alle Daten konnten kopiert werden, da der Tank-Speicher voll ist.");
                                    LEDHandler.setBlink(0, 0.15);
                                    error = true;
                                    break;
                                }
                            }
                            LEDHandler.setBlink(1, 0);
                        }else
                        {
                            Log.writeLine("Keinen Speicher gefunden");
                            LEDHandler.setBlink(1, 0.15, 3);
                        }
                    }
                }
                lck = false;
            }
        }

        private static void button2Pushed(GpioPin sender, GpioPinValueChangedEventArgs e)
        {
            if (e.Edge == GpioPinEdge.FallingEdge && !lck)
            {
                lck = true;
                Log.writeLine("Backup Taste gedrückt");
                if(!error)
                {
                    var tank = DeviceHelper.getTank();
                    var device = DeviceHelper.getBiggestDevice();
                    if (tank == null)
                    {
                        Log.writeLine("Keinen Tank-Speicher gefuden");
                        LEDHandler.setBlink(0, 0.15, 3);
                    }
                    else
                    {
                        if (device != null)
                        {
                            LEDHandler.switchLED(1, true);
                            LEDHandler.setBlink(0, 0.75);
                            var dest = device.CreateFolderAsync("Tank", Windows.Storage.CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
                            Log.writeLine("Kopiere Tank-Speicher auf Gerät: " + DeviceHelper.getName(device));
                            if (!FileOperations.copy(tank, dest, Settings.getSettingBool("deltank")))
                            {
                                Log.writeLine("Nicht alle Daten konnten kopiert werden, da der Speicher voll ist.");
                                LEDHandler.setBlink(1, 0.15);
                                error = true;
                            }
                            LEDHandler.setBlink(0, 0);
                            LEDHandler.switchLED(1, false);
                        }
                        else
                        {
                            Log.writeLine("Keinen Speicher gefuden");
                            LEDHandler.setBlink(1, 0.15, 3);
                        }
                    }
                }
                lck = false;
            }
        }
    }
}
