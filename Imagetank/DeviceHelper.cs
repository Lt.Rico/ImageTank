﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Storage;

namespace Imagetank
{
    class DeviceHelper
    {

        //Sucht den Massenspeicher, auf dem tank.tag ist. Gibt null, falls nicht vorhanden.
        public static StorageFolder getTank()
        {
            var removable = KnownFolders.RemovableDevices.GetFoldersAsync().AsTask().GetAwaiter();

            foreach (var device in removable.GetResult())
            {
                var file = FileOperations.openIfExists(device, "tank.tag");
                if (file != null)
                    return device;
            }
            return null;
        }

        public static String getName(StorageFolder device)
        {
            String name;
            StorageFile file = FileOperations.openIfExists(device, "device.tag");

            if (file != null)
            {
                var reader = new StreamReader(file.OpenReadAsync().AsTask().GetAwaiter().GetResult().AsStreamForRead());
                name = reader.ReadLine();
                reader.Dispose();
            } else
            {
                setName(device, "UNBEKANNT");
                return "UNBEKANNT";
            }
            return name;
        }

        public static List<StorageFolder> getDevices()
        {
            var list = new List<StorageFolder>();
            var sto = KnownFolders.RemovableDevices.GetFoldersAsync().AsTask().GetAwaiter().GetResult();
            var tank = DeviceHelper.getTank();
            foreach (var device in sto)
            {
                if ((tank == null || !tank.Name.Equals(device.Name)) && (device.Attributes & Windows.Storage.FileAttributes.Temporary) != Windows.Storage.FileAttributes.Temporary) //Phantom Geräte des Kartenlesers entfernen
                    list.Add(device);
            }
            return list;
        }

        public static StorageFolder getBiggestDevice()
        {
            var list = getDevices();
            UInt64 i = 0;
            StorageFolder res = null;
            foreach (var device in list)
            {
                var prop = device.Properties.RetrievePropertiesAsync(new string[] { "System.FreeSpace" }).AsTask().GetAwaiter().GetResult();
                UInt64 space = (UInt64)prop["System.FreeSpace"];
                if (space > i)
                {
                    i = space;
                    res = device;
                }
            }
            return res;
        }

        public static void setName(StorageFolder device, String name)
        {
            var file = device.CreateFileAsync("device.tag", CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
            var lines = FileIO.ReadLinesAsync(file).AsTask().GetAwaiter().GetResult();
            if(lines.Count > 0)
                lines.RemoveAt(0);
            lines.Insert(0, name);
            FileIO.WriteLinesAsync(file, lines).AsTask().GetAwaiter();
        }

        public static IList<string> getStartpointList(StorageFolder device)
        {
            if(device.Path.Length > 3)
                device = StorageFolder.GetFolderFromPathAsync(device.Path.Substring(0, 3)).AsTask().GetAwaiter().GetResult();
            var file = device.CreateFileAsync("device.tag", CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
            var lines = FileIO.ReadLinesAsync(file).AsTask().GetAwaiter().GetResult();
            if (lines.Count > 0)
                lines.RemoveAt(0);
            return lines;
        }
        
        public static void addStartpoint(String folder)
        {
            String path = folder.Substring(2);
            folder = folder.Substring(0, 3);
            var device = StorageFolder.GetFolderFromPathAsync(folder).AsTask().GetAwaiter().GetResult();
            var file = device.CreateFileAsync("device.tag", CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
            var lines = FileIO.ReadLinesAsync(file).AsTask().GetAwaiter().GetResult();
            for(int i = 1; i < lines.Count; i++)
            {
                if (lines.ElementAt(i).StartsWith(path))
                    lines.RemoveAt(i);
            }
            lines.Add(path);
            FileIO.WriteLinesAsync(file, lines).AsTask().GetAwaiter();
        }

        public static void removeStartpoint(String folder)
        {
            String path = folder.Substring(2);
            folder = folder.Substring(0, 3);
            var device = StorageFolder.GetFolderFromPathAsync(folder).AsTask().GetAwaiter().GetResult();
            var file = device.CreateFileAsync("device.tag", CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
            var lines = FileIO.ReadLinesAsync(file).AsTask().GetAwaiter().GetResult();
            lines.Remove(path);
            FileIO.WriteLinesAsync(file, lines).AsTask().GetAwaiter();
        }



    }
}
