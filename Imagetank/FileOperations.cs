﻿using System;
using Windows.Storage;

namespace Imagetank
{
    class FileOperations
    {
        
        public static void deleteFolder(StorageFolder folder)
        {
            foreach (var folderInside in folder.GetFoldersAsync().AsTask().GetAwaiter().GetResult())
                deleteFolder(folderInside);

            foreach (var fileInside in folder.GetFilesAsync().AsTask().GetAwaiter().GetResult())
                fileInside.DeleteAsync().AsTask().Wait();

            DeviceHelper.removeStartpoint(folder.Path);
            folder.DeleteAsync().AsTask().Wait();
        }

        public static StorageFile openIfExists(StorageFolder folder, String name)
        {
            try
            {
                var file = folder.GetFileAsync(name).AsTask().GetAwaiter().GetResult();
                return file;
            } catch(Exception)
            {
                return null;
            }
        }

        private static String getNewName(StorageFile file, int i)
        {
            String name = file.DisplayName;
            String extension = file.FileType;
            return name + "_" + i + extension;
        }

        private static String getNewName(StorageFile file, StorageFolder dest)
        {
            var prop1 = file.GetBasicPropertiesAsync().AsTask().GetAwaiter().GetResult();
            var file2 = dest.GetFileAsync(file.Name).AsTask().GetAwaiter().GetResult();
            var prop2 = file2.GetBasicPropertiesAsync().AsTask().GetAwaiter().GetResult();

            if (prop1.DateModified.Equals(prop2.DateModified) && (prop1.Size == prop2.Size))
                return "";

            int i = 0;
            String name = file.Name + "_";
            while((file2 = openIfExists(dest, getNewName(file, ++i))) != null)
            {
                prop2 = file2.GetBasicPropertiesAsync().AsTask().GetAwaiter().GetResult();
                if (prop1.DateModified.Equals(prop2.DateModified) && (prop1.Size == prop2.Size))
                    return "";
            }

            return getNewName(file, i);
        }


        public static bool copy(StorageFolder src, StorageFolder dest, bool delorg)    
        {
            var list = DeviceHelper.getStartpointList(src);
            if(list.Count == 0)
                return copyrec(src, dest, delorg);

            foreach(String s in list)
            {
                var path = src.Path.Substring(0, 2) + s;
                var folder = StorageFolder.GetFolderFromPathAsync(path).AsTask().GetAwaiter().GetResult();
                if (!copyrec(folder, dest, delorg))
                    return false;
            }
            return true;
        }

        private static bool copyrec(StorageFolder src, StorageFolder dest, bool delorg)  //mode: 0=unique 1=replace 2=werfe fehler
        {
            if (src.Name.Equals("WPSystem"))
                return true;
            var folderInside = src.GetFoldersAsync().AsTask().GetAwaiter().GetResult();
            var filesInside = src.GetFilesAsync().AsTask().GetAwaiter().GetResult();
            foreach (var file in filesInside)
            {
                if(!file.FileType.Equals(".tag"))
                {
                    try
                    {
                        file.CopyAsync(dest, file.Name, NameCollisionOption.FailIfExists).AsTask().Wait();
                        if(delorg)
                            file.DeleteAsync().AsTask().Wait();
                    }
                    catch (AggregateException e)
                    {
                        if (e.GetBaseException().GetType() == typeof(System.Runtime.InteropServices.COMException)) //Fehler weil kein Platz auf Ziellaufwerk
                        {
                            return false;
                        }
                        String newName;
                        if(! (newName = getNewName(file, dest)).Equals(""))
                            file.CopyAsync(dest, newName, NameCollisionOption.GenerateUniqueName).AsTask().Wait();
                        if (delorg)
                            file.DeleteAsync().AsTask().Wait();
                    }
                }
            }
            foreach(var folder in folderInside)
            {
                if (!copyrec(folder, dest.CreateFolderAsync(folder.Name, CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult(), delorg))
                    return false;
                if (delorg)
                {
                    try
                    {
                        folder.DeleteAsync().AsTask().Wait();
                    }
                    catch {}
                }
            }
            return true;
        }
    }
}