﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Windows.Storage;

namespace Imagetank
{
    class Helper
    {


        private const String folderThumb = "<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAd5JREFUeNqMU79rFUEQ/vbuodFEEkzAImBpkUabFP4ldpaJhZXYm/RiZWsv/hkWFglBUyTIgyAIIfgIRjHv3r39MePM7N3LcbxAFvZ2b2bn22/mm3XMjF+HL3YW7q28YSIw8mBKoBihhhgCsoORot9d3/ywg3YowMXwNde/PzGnk2vn6PitrT+/PGeNaecg4+qNY3D43vy16A5wDDd4Aqg/ngmrjl/GoN0U5V1QquHQG3q+TPDVhVwyBffcmQGJmSVfyZk7R3SngI4JKfwDJ2+05zIg8gbiereTZRHhJ5KCMOwDFLjhoBTn2g0ghagfKeIYJDPFyibJVBtTREwq60SpYvh5++PpwatHsxSm9QRLSQpEVSd7/TYJUb49TX7gztpjjEffnoVw66+Ytovs14Yp7HaKmUXeX9rKUoMoLNW3srqI5fWn8JejrVkK0QcrkFLOgS39yoKUQe292WJ1guUHG8K2o8K00oO1BTvXoW4yasclUTgZYJY9aFNfAThX5CZRmczAV52oAPoupHhWRIUUAOoyUIlYVaAa/VbLbyiZUiyFbjQFNwiZQSGl4IDy9sO5Wrty0QLKhdZPxmgGcDo8ejn+c/6eiK9poz15Kw7Dr/vN/z6W7q++091/AQYA5mZ8GYJ9K0AAAAAASUVORK5CYII=\" /> ";
        public const String imageThumb = "<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTZEaa/1AAAA1ElEQVQ4T63TPQ4BQRjG8ZUQCoVI3IDGBZxGo1CoXEKn0alVSp24gY5COAA6iVLh8//O7M7K5p3FxpP84p2182x2kg3+lQN2P9qjCJNV+JtDxY4fM0fJjnHBFA90zCo9asEGTwzNKk4eNTu6qAUNdFE2qzg9TOzoohZoqeIEebWWXAiTWiCboowgryWWkIOWeAsKWKOOJq6ICkQbEm9BH3LjDItwfneEnJFaICd9RnJT0gBqwRjahqQLtnAFN8iT79A2+LgC7c9vuAJ5ehbuY8qYIHgBHlFxm5MSumAAAAAASUVORK5CYII=\" /> ";

        public static void init()
        {
            LEDHandler.init();
            ButtonListener.init();
            Log.init();
            Settings.init();
        }

        public static String getFolderHTML(String url)
        {
            String pred = predecessor(url);
            if (!Directory.Exists(url))
            {
                if (!(pred.Trim().Length == 0))
                    return getFolderHTML(pred);
                else
                    return "<tr>\r\n<th colspan=\"3\">Explorer</th>\r\n</tr><tr>\r\n<td>No External Storage found</td>\r\n</tr>\r\n";
            }

            StringBuilder res = new StringBuilder();
            res.Append($"<tr>\r\n<th colspan=\"3\">{url}</th>\r\n</tr>");

            if (pred.Length != 0)
            {
                res.Append($"<tr>\r\n<td><a href=\"/getFolder('{pred}')\">...</a></td>\r\n</tr>\r\n");
            } else
            {
                res.Append("<tr>\r\n<td><a href=\"/index.html\">...</a></td>\r\n</tr>\r\n");
            }

            var folder = StorageFolder.GetFolderFromPathAsync(url).AsTask().GetAwaiter().GetResult();
            var folderInside = folder.GetFoldersAsync().AsTask().GetAwaiter().GetResult();
            var fileInside = folder.GetFilesAsync().AsTask().GetAwaiter().GetResult();

            var startpoints = DeviceHelper.getStartpointList(folder);
            foreach (var file in folderInside)
            {
                if (!file.Name.Equals("WPSystem"))
                {
                    res.Append($"<tr>\r\n<td><a href=\"/getFolder('{url + file.Name}\\')\">{folderThumb + file.Name}/</a></td>\r\n");

                    var i = containsFolder(startpoints, file.Path);
                    if(i == 0)
                        res.Append($"<td><a href=\"/addStart('{url + file.Name}')\">Startpunkt setzen</a></td>\r\n");
                    if(i == 2)
                        res.Append($"<td><a href=\"/removeStart('{url + file.Name}')\">Startpunkt entfernen</a></td>\r\n");
                    if (i == 1)
                        res.Append($"<td></td>\r\n");

                    res.Append($"<td><a href=\"/deleteFile('{url + file.Name}')\" onclick=\"return confirm('Ordner &quot;{file.Name}&quot; löschen?')\">Löschen</a></td>\r\n</tr>\r\n");
                }
            }

            StorageFile[] files = fileInside.ToArray();
            String[] thumbs = ThumbManager.getThumbs(files);
            for (int i = 0; i < files.Length; i++)
            {
                if (!files[i].FileType.Equals(".tag"))
                {
                    res.Append($"<tr>\r\n<td><a href=\"/getFile('{url + files[i].Name}')\">{thumbs[i] + files[i].Name}</a></td><td></td>\r\n");
                    res.Append($"<td><a href=\"/deleteFile('{url + files[i].Name}')\" onclick=\"return confirm('Datei &quot;{files[i].Name}&quot; löschen?')\">Löschen</a></td>\r\n</tr>\r\n");
                } 
            }
            return res.ToString();
        }

        public static String predecessor(String uri)
        {
            StringBuilder res = new StringBuilder();
            var parts = uri.Split('\\');

            if (uri.EndsWith("\\"))
                for (int i = 0; i < (parts.Length - 2); i++)
                    res.Append(parts[i] + "\\");
            else
                for (int i = 0; i < (parts.Length - 1); i++)
                    res.Append(parts[i] + "\\");
            
            return res.ToString();
        }

        private static String getDeviceRowHTML(StorageFolder device)
        {
            if (device == null)
                return "";

            try
            {
                var prop = device.Properties.RetrievePropertiesAsync(new string[] { "System.Capacity", "System.FreeSpace" }).AsTask().GetAwaiter().GetResult();
                UInt64 capacity = (UInt64)prop["System.Capacity"];
                UInt64 space = (UInt64)prop["System.FreeSpace"];
                return $"<td><a href=\"/getFolder('{device.Path}')\">{DeviceHelper.getName(device)}   ('{device.Path}')</a></td><td>{Math.Round(space / 1073741824.0, 2)} GB / {Math.Round(capacity / 1073741824.0, 2)} GB</td>";
            } catch (NullReferenceException)
            {
                return "";
            }
        }

        public static String getDevicesHTML()
        {
            StringBuilder res = new StringBuilder();
            String row;
            var tank = DeviceHelper.getTank();
            
            foreach (var device in DeviceHelper.getDevices())
            {
                row = getDeviceRowHTML(device);
                if (row.Length > 0)
                {
                    res.Append($"<tr>{row}");
                    res.Append($"<td><a href=\"/setName('{device.Path}')\"> Gerät umbenennen</a></td>");
                    res.Append("</tr>");
                }
            }
            row = getDeviceRowHTML(tank);
            if (row.Length > 0)
            {
                res.Append($"<tr>{row}");
                res.Append($"<td><a href=\"/setName('{tank.Path}')\"> Gerät umbenennen</a></td>");
                res.Append("</tr>");
            }


            if (res.Length == 0)
                return "<tr><td>Kein Massenspeicher gefunden.</td></tr>";
            return res.ToString();
        }

        public static String getTankHTML()
        {
            var tank = DeviceHelper.getTank();
            if (tank != null)
            {
                return $"<tr>{getDeviceRowHTML(tank)}<td><a href=\"/setName('{tank.Path}')\"> Gerät umbenennen</a></td></tr>";
            }

            List<StorageFolder> devices = DeviceHelper.getDevices();

            if (devices.Count == 0)
            {
                return "<tr><td>Kein Massenspeicher gefunden.</td></tr>";
            }
            StringBuilder res = new StringBuilder();

            foreach (var device in devices)
            {
                res.Append($"<tr>{getDeviceRowHTML(device)}<td><a href=\"setTank('{device.Path}')\"> Als Speicher setzen</a></td></tr>");
            }
            return res.ToString();

        }
        
        public static String getCheckbox(String value, String text, bool check)
        {
            String chk = "";
            if (check)
                chk = " checked=\"checked\"";
            return $"<input type=\"checkbox\" name=\"settings\" value=\"{value}\"{chk}> {text}<br>";
        }

        private static byte containsFolder(IList<string> list, String path)
        {
            path = path.Substring(2);
            foreach (String s in list)
            {
                if (path.StartsWith(s))
                {
                    if (path.Equals(s))
                        return 2;
                    return 1;
                }
            }
            return 0;
        }

    }



}
