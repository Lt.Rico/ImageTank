﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Gpio;

namespace Imagetank
{
    class LEDHandler
    {

        private const int LED1_PIN = 21;
        private const int LED2_PIN = 19;
        private static GpioPin[] ledPin = new GpioPin[2];
        private static CancellationTokenSource[] tokens = new CancellationTokenSource[2];

        public static void init()
        {
            var gpio = GpioController.GetDefault();
            if (gpio == null)
                return;
            ledPin[0] = gpio.OpenPin(LED1_PIN);
            ledPin[0].Write(GpioPinValue.High);
            ledPin[0].SetDriveMode(GpioPinDriveMode.Output);

            ledPin[1] = gpio.OpenPin(LED2_PIN);
            ledPin[1].Write(GpioPinValue.Low);
            ledPin[1].SetDriveMode(GpioPinDriveMode.Output);

        }

        public static void switchLED(int led, bool on)
        {
            if(on)
                ledPin[led].Write(GpioPinValue.High);
            else
                ledPin[led].Write(GpioPinValue.Low);
        }

        public static void setBlink(int led, double interval)
        {
            if (led < 0 || led >= ledPin.Length)
                return;

            if (interval < 0.001 && tokens[led] != null)
            {
                tokens[led].Cancel();
                tokens[led] = null;
            }

            if(interval > 0.001)
            {
                if(tokens[led] != null)
                {
                    tokens[led].Cancel();
                    tokens[led] = null;
                }
                tokens[led] = new CancellationTokenSource();
                Task t = blink(tokens[led].Token, led, interval);
            }
        }

        public static void setBlink(int led, double interval, int count)
        {
            if (led < 0 || led >= ledPin.Length)
                return;

            for(int i = 0; i < count; i++)
            {
                var value = (ledPin[led].Read() == GpioPinValue.Low) ? GpioPinValue.High : GpioPinValue.Low;
                ledPin[led].Write(value);
                Task.Delay(TimeSpan.FromSeconds(interval)).Wait();
                value = (ledPin[led].Read() == GpioPinValue.Low) ? GpioPinValue.High : GpioPinValue.Low;
                ledPin[led].Write(value);
                Task.Delay(TimeSpan.FromSeconds(interval)).Wait();
            }

        }

        private static Task blink(CancellationToken ct, int led, double interval)
        {
            return Task.Run(() =>
            {
                GpioPinValue value = ledPin[led].Read();
                while (true)
                {
                    ledPin[led].Write((ledPin[led].Read() == GpioPinValue.Low) ? GpioPinValue.High : GpioPinValue.Low);
                    Task.Delay(TimeSpan.FromSeconds(interval)).Wait();
                    try
                    {
                        ct.ThrowIfCancellationRequested();
                    } catch
                    {
                        ledPin[led].Write(value);
                        break;
                    }
                }
            });
        }

    }
}
