﻿using System;
using System.Collections.Generic;
using Windows.Storage;

namespace Imagetank
{
    class Log
    {
        private static IList<string> lines;
        private static StorageFile file;

        public static void init()
        {
            file = ApplicationData.Current.LocalFolder.CreateFileAsync("log.txt", CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
            lines = FileIO.ReadLinesAsync(file).AsTask().GetAwaiter().GetResult();
        }

        public static IList<string> getLog()
        {
            return lines;
        }

        public static void writeLine(String l)
        {
            lines.Add(l);
            FileIO.AppendTextAsync(file, l + "\r\n").AsTask().Wait();
        }

    }
}
