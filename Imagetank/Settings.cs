﻿using System;
using System.Collections.Generic;
using Windows.Storage;

namespace Imagetank
{
    class Settings
    {

        private static Dictionary<String, String> data = new Dictionary<string, string>();
        private static StorageFile file;

        public static void init()
        {

            file = ApplicationData.Current.LocalFolder.CreateFileAsync("settings.txt", CreationCollisionOption.OpenIfExists).AsTask().GetAwaiter().GetResult();
            var s =  FileIO.ReadLinesAsync(file).AsTask().GetAwaiter().GetResult();
            foreach(String st in s)
            {
                data.Add(st.Substring(0, st.IndexOf("=")), st.Substring(st.IndexOf("=")+1));
            }
        }

        public static void setSetting(String name, String val)
        {
            data.Remove(name);
            data.Add(name, val);
            List<String> list = new List<String>();
            foreach (KeyValuePair<String, String> kv in data)
                list.Add($"{kv.Key}={kv.Value}");
            FileIO.WriteLinesAsync(file, list).AsTask().Wait();
        }

        public static String getSetting(String name)
        {
            String res;
            data.TryGetValue(name, out res);
            return res;
        }

        public static bool getSettingBool(String name)
        {
            String res;
            data.TryGetValue(name, out res);
            //Debug.WriteLine(name + "    -    " + res.ToLower().Contains("true"));
            if (res != null && res.ToLower().Contains("true"))
                return true;
            return false;
        }

    }
}
