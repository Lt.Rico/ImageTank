﻿using IotWeb.Common.Http;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;

// The Background Application template is documented at http://go.microsoft.com/fwlink/?LinkID=533884&clcid=0x409

namespace Imagetank
{
    public sealed class StartupTask : IBackgroundTask
    {
        private BackgroundTaskDeferral _deferal;

        public void Run(IBackgroundTaskInstance taskInstance)
        {

            Task.Delay(TimeSpan.FromSeconds(10)).Wait();    //Nötig, da sonst autostart nicht funktioniert

            Helper.init();

            IotWeb.Server.HttpServer server = new IotWeb.Server.HttpServer(80);
            server.AddHttpRequestHandler("/", new IndexHandler());
            server.AddHttpRequestHandler("/log.html", new LogHandler());
            server.AddHttpRequestHandler("/getFolder('", new FolderHandler());
            server.AddHttpRequestHandler("/getFile('", new FileHandler());
            server.AddHttpRequestHandler("/getPicture('", new PictureHandler());
            server.AddHttpRequestHandler("/deleteFile('", new DeleteHandler());
            server.AddHttpRequestHandler("/getSystemResource('", new SystemResourceHandler());
            server.AddHttpRequestHandler("/settings.html", new SettingsHandler());
            server.AddHttpRequestHandler("/setTank('", new TankSetHandler());
            server.AddHttpRequestHandler("/setName('", new NameSetHandler());
            server.AddHttpRequestHandler("/addStart('", new AddStartHandler());
            server.AddHttpRequestHandler("/removeStart('", new RmStartHandler());
            server.Start();

            _deferal = taskInstance.GetDeferral();
            taskInstance.Canceled += TaskInstance_Canceled;
        }


        private void TaskInstance_Canceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            Debug.WriteLine("StartupTask.TaskInstance_Canceled() - {0}", reason.ToString());

            _deferal.Complete();
        }

        private class IndexHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String pretext = File.ReadAllText("preindex.html");
                String posttext = File.ReadAllText("postindex.html");

                String text = "<tr>\r\n<th colspan=\"3\">Explorer</th>\r\n</tr>" + Helper.getDevicesHTML();
                
                var responseBytes = Encoding.UTF8.GetBytes(pretext + text + posttext);
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class LogHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String preText = File.ReadAllText("prelog.html");
                String pastText = File.ReadAllText("postlog.html");
                StringBuilder text = new StringBuilder();
                var lines = Log.getLog();
                for (int i = lines.Count - 1; i > 0; i--)
                    text.AppendLine(lines.ElementAt(i));
                
                var responseBytes = Encoding.UTF8.GetBytes(preText + text.ToString() + pastText);
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class SystemResourceHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String file = uri.Substring(0, uri.IndexOf('\''));
                var responseBytes = File.ReadAllBytes(file);

                response.Headers[HttpHeaders.ContentType] = MimeType.FromExtension(file);
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class FolderHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String folder = uri.Substring(0, uri.IndexOf('\''));
                folder = folder.Replace('/', '\\');

                String pretext = File.ReadAllText("preindex.html");
                String posttext = File.ReadAllText("postindex.html");

                String text = Helper.getFolderHTML(folder);
                var responseBytes = Encoding.UTF8.GetBytes(pretext + text + posttext);
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class FileHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String file = uri.Substring(0, uri.IndexOf('\''));
                file = file.Replace('/', '\\');

                String pretext = File.ReadAllText("preindex.html");
                String posttext = File.ReadAllText("postindex.html");

                StringBuilder text = new StringBuilder($"<tr><td><input type=button value=\"Zurück\" onClick=\"history.go(-1)\"></td>\r\n");
                text.Append($"<td><a href=\"/deleteFile('{file}')\" onclick=\"return confirm('Datei &quot;{file}&quot; löschen?')\"> Löschen</a></td></tr>\r\n");
                
                text.Append($"<tr><td colspan=\"2\"><img src=\"/getPicture('{file}')\"/></td></tr>");

                
                var responseBytes = Encoding.UTF8.GetBytes(pretext + text + posttext);
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class PictureHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String file = uri.Substring(0, uri.IndexOf('\''));
                file = file.Replace('/', '\\');
                var bytes = File.ReadAllBytes(file);

                response.Headers[HttpHeaders.ContentType] = MimeType.FromExtension(file);
                response.Content.Write(bytes, 0, bytes.Length);

            }
        }

        private class DeleteHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String file = uri.Substring(0, uri.IndexOf('\''));
                file = file.Replace('/', '\\');

                Log.writeLine($"Lösche \"{file}\"");

                try
                {
                    var delete = Windows.Storage.StorageFile.GetFileFromPathAsync(file).AsTask().GetAwaiter().GetResult();
                    delete.DeleteAsync().AsTask().GetAwaiter().GetResult();
                    DeviceHelper.removeStartpoint(file);
                }
                catch (ArgumentException) {
                    FileOperations.deleteFolder(Windows.Storage.StorageFolder.GetFolderFromPathAsync(file).AsTask().GetAwaiter().GetResult());
                }
                catch
                {

                }
                
                var responseBytes = Encoding.UTF8.GetBytes($"<html><script>window.location.replace(\"/getFolder('{Helper.predecessor(file).Replace('\\', '/')}')\")</script></html>");
                response.Content.Write(responseBytes, 0, responseBytes.Length);

            }
        }

        private class SettingsHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {

                if(request.QueryString.Contains("ok"))
                {
                    Settings.setSetting("shthumbs", request.QueryString.Contains("shthumbs").ToString());
                    Settings.setSetting("delorg", request.QueryString.Contains("delorg").ToString());
                    Settings.setSetting("deltank", request.QueryString.Contains("deltank").ToString());
                    
                    var responseBytes1 = Encoding.UTF8.GetBytes("<html><script>window.location.replace(\"/settings.html\")</script></html>");
                    response.Content.Write(responseBytes1, 0, responseBytes1.Length);
                    return;
                }

                String pre = File.ReadAllText("presettings.html");
                String post = File.ReadAllText("postsettings.html");
                StringBuilder text = new StringBuilder(Helper.getTankHTML());

                text.Append("</table>\r\n");
                text.Append("<form onsubmit=\"checkForm(this)\" action=\"settings.html\" method=\"get\">\r\n<fieldset>\r\n<legend>Einstellungen</legend>\r\n");
                text.Append(Helper.getCheckbox("shthumbs", "Vorschaubilder anzeigen", Settings.getSettingBool("shthumbs")));
                text.Append(Helper.getCheckbox("delorg", "Originale nach dem Kopieren löschen", Settings.getSettingBool("delorg")));
                text.Append(Helper.getCheckbox("deltank", "Bei Backup Tank löschen", Settings.getSettingBool("deltank")));


                var responseBytes = Encoding.UTF8.GetBytes(pre + text.ToString() + post);
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class TankSetHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {

                String folder = uri.Substring(0, uri.IndexOf('\''));
                folder = folder.Replace('/', '\\');

                try
                {
                    var tank = Windows.Storage.StorageFolder.GetFolderFromPathAsync(folder).AsTask().GetAwaiter().GetResult();
                    tank.CreateFileAsync("tank.tag").AsTask().GetAwaiter().GetResult();
                    Log.writeLine($"Setzt {DeviceHelper.getName(tank)} als Tank-Speicher");
                }
                catch (ArgumentException)
                {
                }

                var responseBytes = Encoding.UTF8.GetBytes("<html><script>window.location.replace(\"/settings.html\")</script></html>");
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class NameSetHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String folder = uri.Substring(0, uri.IndexOf('\''));
                folder = folder.Replace('/', '\\');
                String s = request.QueryString;


                String pretext = File.ReadAllText("preindex.html");
                String posttext = File.ReadAllText("postindex.html");
                String text;

                if (s.Length > 2)
                {
                    s = s.Substring(s.IndexOf("=") + 1, s.IndexOf("&") - 5);
                    try
                    {
                        var device = Windows.Storage.StorageFolder.GetFolderFromPathAsync(folder).AsTask().GetAwaiter().GetResult();
                        Log.writeLine($"Benenne {DeviceHelper.getName(device)} in \"{s}\" um");
                        DeviceHelper.setName(device, s);
                    }
                    catch (ArgumentException)
                    {
                    }
                    var responseBytes1 = Encoding.UTF8.GetBytes("<html><script>window.location.replace(\"/index.html\")</script></html>");
                    response.Content.Write(responseBytes1, 0, responseBytes1.Length);
                    return;
                } else
                {
                    text = "<tr><td>Name für Gerät:</td><td><form onsubmit=\"checkForm(this)\" action=\"')\" method=\"get\"><input type=\"text\" name=\"name\"><input type=\"submit\" name=\"ok\"/>\r\n</form></td></tr>";
                }
                

                var responseBytes = Encoding.UTF8.GetBytes(pretext + text + posttext);
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class AddStartHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String folder = uri.Substring(0, uri.IndexOf('\''));
                folder = folder.Replace('/', '\\');

                DeviceHelper.addStartpoint(folder);
                
                var responseBytes = Encoding.UTF8.GetBytes($"<html><script>window.location.replace(\"/getFolder('{Helper.predecessor(folder).Replace('\\', '/')}')\")</script></html>");
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

        private class RmStartHandler : IHttpRequestHandler
        {
            public void HandleRequest(string uri, HttpRequest request, HttpResponse response, HttpContext context)
            {
                String folder = uri.Substring(0, uri.IndexOf('\''));
                folder = folder.Replace('/', '\\');

                DeviceHelper.removeStartpoint(folder);

                var responseBytes = Encoding.UTF8.GetBytes($"<html><script>window.location.replace(\"/getFolder('{Helper.predecessor(folder).Replace('\\', '/')}')\")</script></html>");
                response.Content.Write(responseBytes, 0, responseBytes.Length);
            }
        }

    }
}
