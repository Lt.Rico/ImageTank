﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.FileProperties;

namespace Imagetank
{
    class ThumbManager
    {

        public static String[] getThumbs(StorageFile[] files)
        {
            String[] res = new String[files.Length];
            if(!Settings.getSettingBool("shthumbs"))
            {
                for (int j = 0; j < res.Length; j++)
                    res[j] = Helper.imageThumb;
                return res;
            }
            int i = files.Length / 4;
            Task[] tasks = new Task[4];
            tasks[0] = generateThumbs(files, res, i * 3, files.Length);
            tasks[1] = generateThumbs(files, res, i * 2, i * 3);
            tasks[2] = generateThumbs(files, res, i, i * 2);
            tasks[3] = generateThumbs(files, res, 0, i);
            foreach (Task t in tasks)
                t.Wait();
            return res;
        }


        private static Task generateThumbs(StorageFile[] files, String[] result, int from, int to)
        {
            return Task.Run(() =>
            {
                for(int i = from; i < to; i++)
                {
                    var thumb = files[i].GetThumbnailAsync(ThumbnailMode.PicturesView, 45).AsTask().GetAwaiter().GetResult();
                    var ms = new MemoryStream();
                    thumb.AsStreamForRead().CopyTo(ms);
                    result[i] = $"<img src=\"data:image/bmp;base64,{Convert.ToBase64String(ms.ToArray())}\" /> ";
                }
            });
        }

    }
}
